package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    //Create a Post
    void createPost(Post post);

    // Viewing all posts
    Iterable<Post> getPosts();

    //Delete a post
    ResponseEntity deletePost(Long id);

    //Update a post
    ResponseEntity updatePost(Long id, Post post);

}
