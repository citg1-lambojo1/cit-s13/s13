package com.zuitt.discussion.services;


import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//Will allow us to use a CRUD repository methods inherited from the CRUDRepository.
@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    //Create Post
    public void createPost(Post post){
        postRepository.save(post);
    }

    //Get all posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //Delete post
    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);
        return new ResponseEntity("Post deleted successfully.", HttpStatus.OK);
    }

    //Update a post
    public ResponseEntity updatePost(Long id, Post post) {
        //Find the post to update
        Post postForUpdate = postRepository.findById(id).get();

        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully.", HttpStatus.OK);
    }

}
