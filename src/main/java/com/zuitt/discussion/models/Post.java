package com.zuitt.discussion.models;

import javax.persistence.*;

@Entity
@Table(name= "posts")
public class Post {

    //Properties
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    //establishes the relationship of the property to the "post" model.
    @ManyToOne

    //sets the relationship to this property and user_id column in the database to the primary key of the user model.
    @JoinColumn(name ="user_id", nullable = false)
    private User user;

    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //Getters and Setter

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
